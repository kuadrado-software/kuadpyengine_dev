from kuadpyengine import Sprite
from kuadpyengine.physics import CollidablePhysicsConfiguration


class Brick(Sprite):
    def __init__(self, animation_set):
        super().__init__(animation_set)
        self.animation_set = animation_set

        self.set_animation("default")
        self.physics_config = CollidablePhysicsConfiguration({
            "motor_power": 5,
            "mass": 4,
            "shock_absorption_coef": 0.8
        })
        self.init_collider_polygon()
