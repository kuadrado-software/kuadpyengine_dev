from kuadpyengine import Sprite
from kuadpyengine.physics import CollidablePhysicsConfiguration


class Ball(Sprite):
    def __init__(self, animation_set, ):
        super().__init__(animation_set)
        self.speed = 5
        self.animation_set = animation_set
        self.set_animation("default")
        self.physics_config = CollidablePhysicsConfiguration({
            "mass": 2,
        })
        self.init_collider_polygon()
