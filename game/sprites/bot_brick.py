from kuadpyengine.automation import BotSprite
from kuadpyengine.physics import CollidablePhysicsConfiguration


class BotBrick(BotSprite):
    def __init__(self, animation_set):
        super().__init__(animation_set)
        self.animation_set = animation_set
        self.set_animation("default")
        self.physics_config = CollidablePhysicsConfiguration({
            "motor_power": 10,
            "mass": 10,
            "shock_absorption_coef": 0.8
        })
        self.init_collider_polygon()
