import pygame
import os
import json
from .image.animation import Animation


class ResourcesManager:
    """
    A ResourceManager instance is responsible of loading the animations resources and holds them in memory 
    as ready to use data structures for sprites, backgrounds, etc.
    """

    def __init__(self):
        self.animations: dict
        self.sounds: dict

    def load_animations(self):
        """
        Loads the project's animation resources from the resource/animations/index.json, 
        creates a dictionary of Animations instances from the files data and write them in the 
        self.animations field.
        """
        anims_index_file = open(os.path.join(
            "resources", "animations", "index.json"))

        anims_index = json.load(anims_index_file)

        animations = dict()
        for key in anims_index.keys():
            animations[key] = (
                [
                    Animation(
                        dict(anim_data, **{
                            "image": pygame.image.load(
                                os.path.join("resources", "animations",
                                             key, anim_data["file"])
                            )
                        })
                    ) for anim_data in anims_index[key]
                ]
            )
        self.animations = animations

    def get_animation_set(self, key):
        """
        Returns a list of animations from the self.animations dict for the given key.
        """
        return self.animations.get(key)
