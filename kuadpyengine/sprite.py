import pygame
from math import sqrt
from typing import Tuple
from .shapes import Rectangle
from .vectors import (
    vec_is_nul,
    vec2,
)
from .image.animation import Animation
from .physics.collidable_physics_configuration import CollidablePhysicsConfiguration
from .physics import G_Modes, Collidable
from .control import Controllable


@Collidable.register
class Sprite(Collidable, Controllable):
    """
    A sprite abstraction holding data for animation, position, physics configuration, etc.
    Sprite is an abstract class and must be extended in order to create a real sprite.
    """

    def __init__(self, animation_set: Tuple[Animation, ...]):
        """
        All the field are declared with their type but not initialized.
        They must be initialized in the extended class.
        """

        super().__init__()

        # A reference of the set of animations available for this sprite.
        self.animation_set = animation_set
        self.animation: Animation  # Abstract, must be set in child class constructor
        self.init_g_state()

    def set_animation(self, key):
        """
        Updates the instance current animation.
        """
        for anim in self.animation_set:
            if anim.name == key:
                self.animation = anim
                return

    def init_collider_polygon(self):
        if not hasattr(self, "animation"):
            raise AttributeError(
                "Animation field must be initialized before collider_polygon")

        super().init_collider_polygon(self.animation.collider)

    def update_position(self):
        """
        Applys the instance's velocity vector state on its position.
        """

        self.fix_min_speed_threshold()

        super().update_position()

    def get_motor(self):
        motor = self.move_request * self.physics_config.motor_power
        if not vec_is_nul(self.world_physics.G):
            # Motor shouldn't hae any effect against gravity
            # TODO implement a jump concept (a different kind of move request ?)
            motor = motor - (self.move_request *
                             abs(motor.dot(self.world_physics.g_orientation)))

        return motor

    def apply_move_request(self):
        """
        Updates velocity by adding a calculated moving motor force
        """
        motor = self.get_motor()
        if not vec_is_nul(motor):
            self.set_velocity(self.velocity + motor)
            self.frame_forces.append(motor)
            self.move_request = vec2()

    def fix_min_speed_threshold(self):
        v = self.velocity
        if vec_is_nul(self.move_request) and v.dot(v) < self.physics_config.min_speed ** 2:
            self.velocity = vec2()

    def update_animation(self):
        """
        Updates the current animation frame
        """
        self.animation.update_frame()
