import pygame
from typing import Tuple
from kuadpyengine import Sprite


class Render:
    """
    Provides a set of static methods to wrap pygame rendering system.
    """

    def draw_game(self, surface: pygame.Surface, sprites: Tuple[Sprite, ...], backgroundImage: pygame.Surface, debug_points, cam_offset=(0, 0)):
        """
        This is the only public method of the class, it must be called with a background image and a set of sprites to draw.
        An optional camera offset position can be provided.
        """
        self.__draw_background(surface, backgroundImage, cam_offset)
        self.__draw_sprites(surface, sprites, cam_offset)

        self.__draw_debug(surface, cam_offset, debug_points)
        pygame.display.flip()

    def __draw_background(self, surface, image, cam_offset):
        """
        draw_s a background image on the window.
        """
        surface.blit(
            image, (0 + cam_offset[0], 0 + cam_offset[1]), image.get_rect())

    def __draw_sprites(self, surface, sprites, cam_offset):
        """
        Draw a set of sprites on the windows.
        """
        for sprite in sprites:
            surface.blit(sprite.animation.image,
                         (sprite.position[0] + cam_offset[0],
                          sprite.position[1] + cam_offset[1]),
                         sprite.animation.get_frame_rect())

    def __draw_debug(self, surface, cam_offset, points):
        for p in points:
            pygame.draw.rect(surface, (255, 0, 0),
                             (p[0] + cam_offset[0], p[1] + cam_offset[1], 5, 5))
