from typing import Tuple
from kuadpyengine.shapes import Rectangle


class Camera:
    def __init__(self, focus: Rectangle, bounds: Rectangle):
        self.bounds = bounds
        self.focus = focus
        self.position = (0, 0)

    def follow(self, position: Tuple[int, int]):
        cam_pos = [
            position[0] - self.focus.width / 2,
            position[1] - self.focus.height / 2
        ]

        if cam_pos[0] < 0:
            cam_pos[0] = 0
        elif cam_pos[0] + self.focus.width > self.bounds.right:
            cam_pos[0] = self.bounds.right - self.focus.width

        if cam_pos[1] < 0:
            cam_pos[1] = 0
        elif cam_pos[1] + self.focus.height > self.bounds.bottom:
            cam_pos[1] = self.bounds.bottom - self.focus.height

        self.position = cam_pos
