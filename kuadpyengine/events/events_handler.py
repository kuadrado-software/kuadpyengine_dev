import pygame
import sys


class EventsHandler:
    """
    Provides a set of methods to handle window events.
    """

    def __init__(self, listeners=dict()):
        """
        Listeners must implement the abstract EventListener class
        """
        self.listeners = listeners

    def handle_events_queue(self, events):
        """
        Parse the window event queue and call the appropriate event listeners
        """
        for e in events:
            if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                for l in self.listeners.get("quit", ()):
                    l.handle_event(e)
            elif (e.type == pygame.KEYDOWN or e.type == pygame.KEYUP):
                for l in self.listeners.get("keyboard", ()):
                    l.handle_event(e)
