from time import time


class FrameRateController:
    """
    Provides a set of methods to control the frame rate of a game loop.
    """

    def __init__(self, fps):
        """
        An instance of FrameRateController will be initiallized with a frame per second value,
        as its interval attribute.
        """
        self.tframe = self.get_time()
        self.interval = 1000 / fps

    def get_time(self):
        """
        Returns current time in milliseconds.
        """
        return int(time() * 1000)

    def next_frame_ready(self) -> bool:
        """
        Returns true if self.interval is elapsed since last update
        """
        now = self.get_time()
        elpased = now - self.tframe
        ready = elpased >= self.interval
        if ready:
            self.tframe = now - (elpased % self.interval)
        return ready
