from kuadpyengine import Sprite
from kuadpyengine.numbers import MAX_FLOAT
from kuadpyengine.vectors import (
    get_magnitude,
    get_unit_vec,
    vec2
)
import numpy as np


class PatternPlayerOptions:
    def __init__(self, data):
        self.positioning = data.get("positioning", "default")
        self.wait_interval = data.get("wait_interval", 0)
        # ... WIP


class BotSprite(Sprite):
    def __init__(self, animation_set):
        super().__init__(animation_set)
        self.target_index = 0

        # will be dot product of velocity and th moving direction. Sign will change if target is reached
        self.direction_request = vec2()

        self.pattern = tuple()
        self.initiated = False
        self.must_change_target = False
        self.pattern_player = {
            "loop": self.__play_loop,
            # ... more pattern types
        }
        self.player_options = PatternPlayerOptions(dict())
        self.wait_frames = 0

    def pattern_error_handler(self, __pattern, key):
        raise KeyError("Pattern " + key + " is undefined")

    def get_position_option(self):
        switcher = {
            "default": self.position,
            "center": self.get_center_position(),
        }
        return switcher[self.player_options.positioning]

    def get_pattern_closest_point_index(self, points):
        min_dist = MAX_FLOAT
        closest_point_index = 0
        position = self.get_position_option()
        for (i, pt) in enumerate(points):
            # If pattern changes, choose the closest point as a starting point
            # or specify in options param
            dif = position - pt
            dist = dif.dot(dif)
            if dist < min_dist:
                min_dist = dist
                closest_point_index = i
        return closest_point_index

    def update_direction_request(self, points):
        target_pt = points[self.target_index]
        p = self.get_position_option()
        self.direction_request = get_unit_vec(target_pt - p)
        dir_req = self.direction_request
        self.direction_sign = -1 if (p - target_pt).dot(dir_req) < 0 else 1

    def play_auto_move_sequence(self, points: np.array, mode, options=PatternPlayerOptions(dict())):
        # TODO handle different type of sequence: loop, go_to, random, etc
        # Support script injection in pattern ?
        if not self.initiated:
            self.pattern = points
            self.player_options = options
            self.target_index = self.get_pattern_closest_point_index(points)
            self.update_direction_request(points)
            self.initiated = True

        if (points == self.pattern).all():
            self.pattern = points
            self.player_options = options
            self.target_index = self.get_pattern_closest_point_index(points)
            self.update_direction_request(points)

        self.pattern_player.get(mode, self.pattern_error_handler)(points, mode)
        self.apply_move_request()

    def __play_loop(self, points, __key):
        tgt = points[self.target_index]
        p = self.get_position_option() + self.velocity
        dif = tgt - p if self.direction_sign < 0 else p - tgt

        if dif.dot(self.direction_request) <= 0:
            wait_on_position = False
            if self.player_options.wait_interval > 0:
                if self.wait_frames < self.player_options.wait_interval:
                    self.wait_frames += 1
                    wait_on_position = True
                else:
                    self.wait_frames = 0
                    wait_on_position = False

            if wait_on_position:
                self.push_move_request(vec2())
            else:
                prev_index = self.target_index
                self.target_index = prev_index + 1 \
                    if prev_index + 1 <= len(points) - 1 \
                    else 0
                self.update_direction_request(points)
        else:
            self.push_move_request(self.direction_request)
