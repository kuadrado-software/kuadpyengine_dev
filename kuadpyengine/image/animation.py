import pygame
import numpy as np
from kuadpyengine.shapes import Rectangle
from kuadpyengine.frame_rate import FrameRateController
from kuadpyengine.vectors import vec2


class Animation:
    """
    Provides an abstraction of an animation. Holds convenient animation data like frame_count, size, name, collider, etc.
    """

    def __init__(self, anim_data: dict):
        self.name = anim_data["name"]
        self.frame_count = anim_data["frame_count"]
        self.fps = anim_data["fps"]

        # needs to be float type because may be scaled to a non int
        self.dimensions = np.array(anim_data["dimensions"], dtype=float)
        self.collider = np.array(anim_data["collider"], dtype=float)

        self.flip = anim_data.get("flip", (False, False))
        self.image: pygame.Surface = pygame.transform.flip(
            anim_data["image"], self.flip[0], self.flip[1])
        self.current_frame = 0
        self.fps_controller = FrameRateController(self.fps or 50)

    def update_frame(self):
        """
        Update the current frame index to use.
        """
        if self.fps_controller.next_frame_ready() and self.frame_count > 1 and self.fps > 0:
            self.current_frame = 0 if self.current_frame + \
                1 >= self.frame_count else self.current_frame + 1

    def get_frame_position(self):
        """
        Returns the sprite position regarding the animation frame index and the animation size
        """
        return self.current_frame * self.dimensions[0]

    def get_frame_rect(self):
        """
        Returns a rectangle's coordinates from the current animation frame.
        """
        w, h = self.dimensions
        return (self.get_frame_position(), 0, w, h)

    def scale(self, factor):
        im_rect = self.image.get_rect()
        size = vec2(im_rect.width, im_rect.height) * factor
        self.image = pygame.transform.scale(self.image, size)
        self.dimensions = self.dimensions * factor
        self.collider = self.collider * factor
