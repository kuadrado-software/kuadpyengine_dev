import pygame
from kuadpyengine.shapes import Rectangle


class Background:
    def __init__(self, surface:pygame.Surface):
        self.image = surface
        self.rect = Rectangle(self.image.get_rect())
