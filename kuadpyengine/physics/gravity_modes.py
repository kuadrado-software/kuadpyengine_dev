from kuadpyengine.vectors import vec2


class G_Modes:
    TOP_VIEW = vec2(0, 0)
    SIDE_VIEW = vec2(0, 1)
    UPSIDE_DOWN_VIEW = vec2(0, -1)
