from itertools import zip_longest
from typing import Tuple
import numpy as np


class Polygon:
    def __init__(self, points: np.array):
        self.points = points

        # There must be a better way to do this with numpy ...
        self.segments = np.array(tuple(self.get_segments()))

    def get_segments(self):
        pts_len = len(self.points)
        for i in range(0, pts_len):
            yield (self.points[i], self.points[i + 1 if i + 1 < pts_len else 0])

    def scale(self, factor):
        self.points = self.points * factor
        self.segments = self.segments * factor
