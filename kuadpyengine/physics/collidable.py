from typing import List, Tuple, Set
from functools import reduce
from .collidable_physics_configuration import CollidablePhysicsConfiguration
from .polygon import Polygon
from .world_physics import WorldPhysics
from .gravity_modes import G_Modes
from kuadpyengine.vectors import (
    get_unit_vec,
    get_normal_vecs,
    get_magnitude,
    vec_is_nul,
    get_sign_vec,
    get_vecs_list_addition,
    vec2,
    vec_is_in_list
)
from kuadpyengine.numbers import MAX_FLOAT
import abc
import numpy as np


class CollisionDescriptor:
    def __init__(self, relative_velocity, min_response, collidable, normal_vec):
        self.relative_velocity = relative_velocity
        self.min_response = min_response
        self.collidable = collidable
        self.normal_vec = normal_vec


class Collidable(abc.ABC):
    def __init__(self):
        super().__init__()
        self.position = vec2()
        self.collision_impulse_stack = list()
        self.mobility_constraint_stack = list()
        self.velocity = vec2()
        self.frame_forces = list()

        # Will be set in constructor regarding sprite's mass and gravity orientation
        self.g_state = vec2()

        # Provide a default physics config
        self.physics_config = CollidablePhysicsConfiguration({})
        self.submit_to_physical_world()  # init to default global physics settings

        # create a list of points with coordinates relative to position
        self.collider_polygon: Polygon

        # Coordinates x y for the point at the center of mass of the shape
        self.centroid: np.array

        # the largest distance between the center and the edges of the polygon.
        self.pre_collision_ray: float

        self.collides_with: Set[Collidable] = set()
        self.collision_events = list()

        # Generate a unique id for when we have no interest in using a real reference of the object
        self.uid = id(self)
        self.name = type(self).__name__

    def set_name(self, name):
        self.name = name

    @abc.abstractmethod
    def init_collider_polygon(self, points: np.array):
        self.collider_polygon = Polygon(points)
        sum_points = vec2()
        for pt in points:
            sum_points = sum_points + pt

        self.centroid = sum_points * (1 / len(points))

        cm = self.centroid
        max_ray = 0
        for pt in points:
            dist = get_magnitude(pt - cm)
            if dist > max_ray:
                max_ray = dist
        self.pre_collision_ray = max_ray

    def init_g_state(self):
        g_dir = self.world_physics.g_orientation
        g_acc = self.world_physics.g_acceleration
        self.g_state = g_dir * g_acc

    def set_velocity(self, v):
        c_stack = self.mobility_constraint_stack
        if len(c_stack) > 0:
            self.mobility_constraint_stack = [
                c for c in c_stack if c.dot(v) >= 0]
        self.velocity = v

    def push_velocity_correction(self, vec):
        self.set_velocity(self.velocity + vec)

    def apply_gravity(self):
        if not vec_is_nul(self.world_physics.G):
            self.set_velocity(self.velocity + self.world_physics.G)
            self.frame_forces.append(self.world_physics.G)

    def apply_inertia(self):
        v = self.velocity
        m = self.physics_config.mass

        if not vec_is_nul(self.world_physics.G):
            # We don't apply inertia in gravity direction so we
            # substract the dot product of velocity in that direction (minus the base acceleration)
            dot_v_g = v.dot(self.world_physics.g_orientation) - \
                self.world_physics.g_acceleration
            v = v - get_unit_vec(v) * dot_v_g

        v_len = get_magnitude(v)
        u = vec2() if v_len == 0 else v / v_len
        inertia = -v + (u * (v_len / m))

        if not vec_is_nul(inertia):
            self.set_velocity(self.velocity + inertia)
            self.frame_forces.append(inertia)

    def push_collision_with(self, collidable):
        if self.physics_config.movable != 0:
            self.collides_with.add(collidable)

    def push_mobility_constraint(self, constraint):
        if not vec_is_in_list(constraint, self.mobility_constraint_stack):
            self.mobility_constraint_stack.append(constraint)

    def get_mobility_constraint_cumul(self):
        return get_sign_vec(get_vecs_list_addition(self.mobility_constraint_stack))

    def push_collision_response_data(self, data):
        self.collision_impulse_stack.append(data)

    def handle_collision_detection_with_group(self, collidable_group, options={"response_mode": "none"}):
        collidable_group: List[Collidable] = filter(
            lambda x: not x is self, collidable_group)
        if not vec_is_nul(self.velocity):
            collidable_group = self.get_directional_raycasted_collidable_group(
                collidable_group)
        else:
            collidable_group = self.get_circular_raycasted_collidable_group(
                collidable_group)
        for c2 in collidable_group:
            if not self.is_colliding_with(c2):
                self.handle_collision_detection(c2, options)

    def get_polygon_projection(self, axis):
        proj_points = np.array([(pt + self.position).dot(axis)
                                for pt in self.collider_polygon.points])
        return vec2(np.min(proj_points), np.max(proj_points))

    def get_proj_velocity_extrapolation(self, proj, axis, velocity, extrapolation=None) -> Tuple[float, np.array]:
        extrapolation = extrapolation if extrapolation else abs(
            axis.dot(velocity))

        # Get c1 and c2 velocity projections
        v_proj = axis.dot(velocity)
        abs_v_proj = abs(v_proj)
        offset = 0

        if abs_v_proj > abs(proj[1] - proj[0]):
            proj = vec2(proj[0] - extrapolation,
                        proj[1] + extrapolation)
            offset = extrapolation
        elif v_proj < 0:
            proj = vec2(proj[0] - extrapolation,
                        proj[1])
        else:
            proj = vec2(proj[0],
                        proj[1] + extrapolation)

        return (offset, proj)

    def get_projections_interval_distance(self, proj_1, proj_2):
        min_a = proj_1[0]
        max_a = proj_1[1]
        min_b = proj_2[0]
        max_b = proj_2[1]
        if min_a < min_b:
            return min_b - max_a
        else:
            return min_a - max_b

    def get_directional_raycasted_collidable_group(self, collidable_group):
        ray_casted_group = list()

        # Get an axis perpendicular to velocity
        v_norm = get_unit_vec(get_normal_vecs(self.velocity)[0])

        # Get an axis parallel to velocity
        v_axis = get_unit_vec(self.velocity)

        v1 = self.velocity

        c1_proj = self.get_polygon_projection(v_norm)

        (_offset_proj_c1, c1_proj) = self.get_proj_velocity_extrapolation(
            c1_proj, v_norm, v1)

        c1_v_proj = self.get_polygon_projection(v_axis)

        (offset_v_proj_c1, c1_v_proj) = self.get_proj_velocity_extrapolation(
            c1_v_proj, v_axis, v1)

        for c2 in collidable_group:
            c2_proj = c2.get_polygon_projection(v_norm)
            (_offset_proj_c2, c2_proj) = self.get_proj_velocity_extrapolation(
                c2_proj, v_norm, c2.velocity)

            d = self.get_projections_interval_distance(c1_proj, c2_proj)

            if d < 0:
                v2 = c2.velocity
                c2_v1_proj = c2.get_polygon_projection(v_axis)
                (offset_v_proj_c2, c2_v1_proj) = self.get_proj_velocity_extrapolation(
                    c2_v1_proj, v_axis, v2, v2.dot(v_axis))

                distance = c2_v1_proj[0] - c1_v_proj[1] + \
                    offset_v_proj_c1 + offset_v_proj_c2
                min_distance = c2_v1_proj[1] - c1_v_proj[0]
                if min_distance > 0 and distance < self.pre_collision_ray + c2.pre_collision_ray:
                    result = {"collidable": c2, "distance": distance}
                    ray_casted_group.append(result)

        ray_casted_group.sort(key=lambda r: r["distance"])
        return [r["collidable"] for r in ray_casted_group]

    def get_circular_raycasted_collidable_group(self, collidable_group):
        """
        Returns the portion of the paramter collidable group wich is in the potential collision area of the instance
        """
        c1_center_pos = self.get_center_position()

        def is_in_collision_area(collidable):
            c2 = collidable
            distance = c1_center_pos - c2.get_center_position()
            axis = get_unit_vec(distance)
            min_dist = c2.pre_collision_ray + \
                axis.dot(c2.velocity) + self.pre_collision_ray

            tolerance_offset = 0
            return get_magnitude(distance) < min_dist + tolerance_offset

        return [c for c in collidable_group if is_in_collision_area(c)]

    def handle_collision_detection(self, collidable, options):
        """
        Separating Axis Theorem collision detection
        """
        c2: Collidable = collidable
        min_distance = MAX_FLOAT
        separating_vec = vec2()
        intersect = True
        will_intersect = True
        v1 = self.velocity
        v2 = c2.velocity
        polys_center_vec = self.get_center_position() - c2.get_center_position()

        for seg in np.concatenate((self.collider_polygon.segments, c2.collider_polygon.segments)):
            # Create the projection axis for that segment
            axis = get_unit_vec(
                get_normal_vecs(
                    seg[1] - seg[0]
                )[0]
            )

            # Get collidable c1 projection on axis
            proj_c1 = self.get_polygon_projection(axis)

            proj_c2 = c2.get_polygon_projection(axis)

            # Check distance between projections of the 2 polygons
            distance = self.get_projections_interval_distance(proj_c1, proj_c2)

            if distance > 0:
                intersect = False

            # Update projections with velocity extrapolation.
            v_dif_proj = axis.dot(v1 - v2)

            abs_v_dif_proj = abs(v_dif_proj)

            (offset_proj_c1, proj_c1) = self.get_proj_velocity_extrapolation(
                proj_c1, axis, v1, abs_v_dif_proj)

            (offset_proj_c2, proj_c2) = self.get_proj_velocity_extrapolation(
                proj_c2, axis, v2, abs_v_dif_proj)

            # Check distance again to detect a future collision with velocity extrapolated

            add_offset = 0
            if not vec_is_nul(v1) and offset_proj_c2 != 0:
                add_offset += offset_proj_c2
            if not vec_is_nul(v2) and offset_proj_c1 != 0:
                add_offset += offset_proj_c1

            distance = self.get_projections_interval_distance(
                proj_c1, proj_c2) + add_offset

            if distance > 0:
                will_intersect = False

            if not intersect and not will_intersect:
                break

            # If the distance is the smallest we found so far, we update the response vector
            distance = abs(distance)
            if distance < min_distance:
                min_distance = distance
                separating_vec = axis

        if will_intersect:
            if options["response_mode"] == "solid":
                min_response = separating_vec * min_distance
                n = separating_vec

                # Calculate the minium response vector to get out of collision
                centers_vec_scale = polys_center_vec.dot(n)

                if centers_vec_scale < 0:
                    min_response_c1 = -min_response
                    n1 = -n
                    n2 = n
                else:
                    min_response_c1 = min_response
                    n1 = n
                    n2 = -n

                # if c1 velocity projection along collision normal is larger than c2's,
                # v1·n2 will be greater than v2·n1
                relative_velocity = v1 - \
                    v2 if v1.dot(n2) > v2.dot(n1) else v2 - v1

                self.push_collision_response_data(CollisionDescriptor(
                    relative_velocity, min_response_c1, c2, n1))

                c2.push_collision_response_data(CollisionDescriptor(
                    relative_velocity, vec2(), self, n2))

                if c2.physics_config.movable == 0:
                    self.push_mobility_constraint(n1)

            self.push_collision_with(c2)
            c2.push_collision_with(self)

    def apply_collisions(self):
        impulse = vec2()
        mob_constraint_cumul = self.get_mobility_constraint_cumul()
        nul_constraint = vec_is_nul(mob_constraint_cumul)

        for item in self.collision_impulse_stack:
            c2: Collidable = item.collidable
            c2_mov = c2.physics_config.movable
            n = item.normal_vec
            v_dif = item.relative_velocity
            v_transfer_scale = v_dif.dot(n) * c2_mov
            v_transfer = n * v_transfer_scale
            nul_transfer = vec_is_nul(v_transfer)
            nul_response = vec_is_nul(item.min_response)

            # print("APPLY COLLISION", self.get_name(), c2.get_name())
            # print("   got transfer", v_transfer)
            # print("   got min response", item.min_response)
            # print("   has current velocity", self.velocity)

            if not nul_transfer:
                if not nul_constraint:
                    v_transfer += n * mob_constraint_cumul.dot(v_transfer)

                if len(c2.mobility_constraint_stack) > 0:
                    cumul = c2.get_mobility_constraint_cumul()
                    v_transfer -= n * cumul.dot(v_transfer)

            if not nul_response:
                c2_impulse_cumul = get_vecs_list_addition(
                    [c.min_response for c in c2.collision_impulse_stack if not (c.collidable is self)])

                if not vec_is_nul(c2_impulse_cumul):
                    impulse_correction = item.normal_vec * \
                        item.normal_vec.dot(c2_impulse_cumul)
                    impulse += impulse_correction

                if not nul_constraint and not vec_is_nul(c2.velocity):
                    impulse_correction = n * \
                        mob_constraint_cumul.dot(item.min_response)

                    impulse += impulse_correction
                    c2.push_velocity_correction(impulse_correction)

            impulse += item.min_response
            impulse += v_transfer

        if not vec_is_nul(impulse):
            self.set_velocity(self.velocity + impulse)

    def is_colliding_with(self, collidable):
        return collidable in self.collides_with

    def flush_collisions(self):
        if len(self.collides_with) > 0:
            self.collides_with = set()
            self.collision_impulse_stack = list()

    def flush(self):
        self.frame_forces = list()
        self.flush_collisions()

    def submit_to_physical_world(self, world_physics=WorldPhysics({"g_orientation": G_Modes.TOP_VIEW})):
        self.world_physics = world_physics

    def set_physics_config(self, physics_config: CollidablePhysicsConfiguration):
        """
        Sets the physics_config field of the instance with a CollidablePhysicsConfiguration object
        """
        self.physics_config = physics_config

    def set_position(self, x: float, y: float):
        """
        Update the instance's position with x y parameters
        """
        self.position = vec2(x, y)

    def get_center_position(self):
        """
        Returns the instance's center position
        """
        return self.centroid + self.position

    def update_position(self):
        """
        Applys the instance's velocity vector state on its position.
        """
        self.position = self.position + self.velocity

    def get_name(self):
        return getattr(self, "name", type(self).__name__)
