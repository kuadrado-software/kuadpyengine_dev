from typing import Tuple
from .gravity_modes import G_Modes
import numpy as np


class WorldPhysics:
    def __init__(self, data):
        # g_orientation is represented by a 2D unit vector
        self.g_orientation = np.array(data.get(
            "g_orientation", G_Modes.TOP_VIEW))
        self.g_acceleration = data.get("g_acceleration", 7)
        self.G = self.g_orientation * self.g_acceleration
        # could initialize and update data such as wind etc... TBD
