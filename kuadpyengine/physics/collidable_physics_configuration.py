class CollidablePhysicsConfiguration:
    def __init__(self, data):
        self.min_speed = data.get("min_speed", .3)
        self.motor_power = data.get("motor_power", 1)
        self.shock_absorption_coef = data.get("shock_absorption_coef", .3)

        # 1 or 0. Will be use as a factor of mass in collision equations
        self.movable = int(data.get("movable", True))

        self.mass = 1 + data.get("mass", 10) / 10
        if self.mass < 0:
            raise ValueError("Mass must be >= 0")
