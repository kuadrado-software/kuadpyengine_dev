from math import sqrt
from functools import reduce
import numpy as np

NUL_FLOAT_THRESHOLD = 0.0000001

"""
2D vector operations
All vectors must be numpy arrays of length 2
"""


def get_unit_vec(vector):
    """
    Get's a 2D vector as an argument an returns the corresponding unit vector.
    # """
    sqr_magn = vector.dot(vector)

    if sqr_magn == 0:
        return vec2()

    vector_magn = np.sqrt(sqr_magn)
    return vector / vector_magn


def get_sign_vec(vector):
    return np.sign(vector).astype(np.float)


def get_magnitude(vector):
    return np.sqrt(vector.dot(vector))


def has_unit_length(vector):
    return abs(1 - vector.dot(vector)) < NUL_FLOAT_THRESHOLD


def float_is_nul(number):
    return abs(number) < NUL_FLOAT_THRESHOLD


def vec2(n1=0.0, n2=0.0) -> np.array:
    return np.array([n1, n2])


def vec_is_nul(vector):
    return float_is_nul(vector.dot(vector))


def get_normal_vecs(v):
    """
    Return 2 vectors for the 2 possible direction of a normal vector to v.
    """
    return (
        vec2(-v[1], v[0]),
        vec2(v[1], -v[0])
    )


def get_vecs_list_addition(vectors):
    return reduce(lambda total, vec: total + vec, vectors, vec2())


def vec_is_in_list(vec, vecs_list):
    if (len(vecs_list) == 0):
        return False
    for v in vecs_list:
        if np.array_equal(vec, v):
            return True
    return False
