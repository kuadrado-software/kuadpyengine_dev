import sys
import os
import json
import pygame
import numpy as np
from random import randint
from kuadpyengine import (
    FrameRateController,
    ResourcesManager,
)
from kuadpyengine.physics import (
    WorldPhysics,
    G_Modes,
    CollidablePhysicsConfiguration,
)
from kuadpyengine.image import Background
from kuadpyengine.render import Camera, Render
from kuadpyengine.shapes import Rectangle, Segment
from kuadpyengine.control.inputs import QuadridirectionalInput
from kuadpyengine.events import EventListener, EventsHandler
from kuadpyengine.vectors import vec_is_nul, vec2
from kuadpyengine.automation import PatternPlayerOptions
from game.sprites import Ball, Brick, BotBrick

pygame.init()

screen_rect = Rectangle(pygame.Rect(0, 0, 1000, 600))
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
pygame.display.set_caption("Kuadpyengine Dev")

resources_manager = ResourcesManager()
resources_manager.load_animations()

world_physics = WorldPhysics(
    {"g_orientation": G_Modes.TOP_VIEW})

big_map = Background(resources_manager.get_animation_set("big_map")[0].image)

balls = [Ball(resources_manager.get_animation_set("ball"))
         for ball in range(0, 0)]

for ball in balls:
    # ball.set_position(randint(0, big_map.rect.width - ball.animation.dimensions[0]),
    #                   randint(0, big_map.rect.height - ball.animation.dimensions[1]))
    ball.set_position(1000, 500)
    ball.submit_to_physical_world(world_physics)

brick = Brick(resources_manager.get_animation_set("brick"))
brick.set_position(100, 100)
brick.submit_to_physical_world(world_physics)
brick.set_name("Brick_1")

brick2 = Brick(resources_manager.get_animation_set(
    "brick"))
brick2.set_position(500, 800)
brick2.submit_to_physical_world(world_physics)
brick2.set_name("Brick_2")

auto_brick = BotBrick(resources_manager.get_animation_set("brick"))
auto_brick.set_position(2000, 400)
auto_brick.submit_to_physical_world(world_physics)
# auto_brick.set_physics_config(CollidablePhysicsConfiguration({
#     "movable": False,
# }))
auto_brick.set_name("Brick 3")

map_edges = [
    Segment(vec2(0.0, big_map.rect.height), vec2()),  # left
    Segment(vec2(), vec2(big_map.rect.width, 0.0)),  # top
    Segment(vec2(big_map.rect.width, 0.0),
            vec2(big_map.rect.width, big_map.rect.height)),  # right
    Segment(vec2(big_map.rect.width, big_map.rect.height),
            vec2(0.0, big_map.rect.height - 500)),  # bottom
]

map_edges[0].set_name("Map edge left")
map_edges[1].set_name("Map edge top")
map_edges[2].set_name("Map edge right")
map_edges[3].set_name("Map edge bottom")


for edge in map_edges:
    edge.set_physics_config(CollidablePhysicsConfiguration({
        "movable": False,
    }))

arrows_input = QuadridirectionalInput()

camera = Camera(Rectangle(pygame.Rect((0, 0), screen.get_size())),
                big_map.rect)


class Debuger:
    def __init__(self):
        self.points = list()

    def push_point(self, point):
        self.points.append(point)


debuger = Debuger()

render = Render()

quit_game = False


@ EventListener.register
class QuitGameHandler(EventListener):
    def handle_event(self, _e):
        global quit_game
        quit_game = True
        print("bye bye")


quit_game_handler = QuitGameHandler()

events_handler = EventsHandler(listeners={
    "keyboard": (
        arrows_input,
    ),
    "quit": (
        quit_game_handler,
    )
})

fps_controller = FrameRateController(30)


def get_visible_balls():
    return list(filter(lambda ball: ball.position[0] + ball.animation.dimensions[0] > camera.position[0] and
                       ball.position[0] < screen_rect.width + camera.position[0] and
                       ball.position[1] + ball.animation.dimensions[1] > camera.position[1] and
                       ball.position[1] < camera.position[1] + screen_rect.height, balls)
                )


brick_pattern = np.array([
    [40, 40],
    [500, 40],
    [500, 500],
    [30, 500]
])

while not quit_game:

    if fps_controller.next_frame_ready():

        events_handler.handle_events_queue(pygame.event.get())

        brick.push_move_request(arrows_input.state)
        brick.apply_move_request()

        auto_brick.play_auto_move_sequence(
            brick_pattern, "loop", PatternPlayerOptions({"wait_interval": 30}))

        brick.apply_gravity()
        brick2.apply_gravity()
        auto_brick.apply_gravity()

        brick.apply_inertia()
        brick2.apply_inertia()
        auto_brick.apply_inertia()

        for ball in balls:
            ball.apply_gravity()
            ball.apply_inertia()
            ball.handle_collision_detection_with_group([*map_edges,
                                                        *filter(lambda o: vec_is_nul(o.velocity), [brick, brick2, auto_brick]),
                                                        *get_visible_balls()], {"response_mode": "solid"})

        brick.handle_collision_detection_with_group([*map_edges,
                                                     brick2, auto_brick,
                                                     *get_visible_balls()], {"response_mode": "solid"})

        # brick.handle_collision_detection_with_group([auto_brick])

        brick2.handle_collision_detection_with_group([*map_edges,
                                                      brick, auto_brick,
                                                      * get_visible_balls()], {"response_mode": "solid"})

        auto_brick.handle_collision_detection_with_group([*map_edges,
                                                          brick, brick2,
                                                          * get_visible_balls()], {"response_mode": "solid"})

        brick.apply_collisions()
        brick2.apply_collisions()
        auto_brick.apply_collisions()

        # if brick.is_colliding_with(brick2):
        #     print("PONK !")
        # if brick.is_colliding_with(auto_brick):
        #     print("BING !")

        # if brick2.is_colliding_with(auto_brick):
        #     print("BUMP !")

        for ball in balls:
            ball.apply_collisions()

            # if brick.is_colliding_with(ball):
            #     print("TINK !")
            ball.fix_min_speed_threshold()
            ball.update_position()
            ball.update_animation()
            ball.flush()

        brick.fix_min_speed_threshold()
        brick.update_position()
        brick.update_animation()

        brick2.fix_min_speed_threshold()
        brick2.update_position()
        brick2.update_animation()

        auto_brick.fix_min_speed_threshold()
        auto_brick.update_position()
        auto_brick.update_animation()

        brick.flush()
        brick2.flush()
        auto_brick.flush()

        camera.follow(brick.get_center_position())

        render.draw_game(screen,
                         (auto_brick, brick2, brick, *balls),
                         big_map.image,
                         debuger.points,
                         (-camera.position[0], -camera.position[1]))
